// ==UserScript==
// @name         Google Maps Worktime Collector
// @namespace    anton-olsson@outlook.com
// @version      0.3
// @description  jQuery is a prequistie which is loaded automatically. Open console, walk the dates and watch the magic happen. Saves history as CSV.
// @author       Anton
// @match        https://www.google.com/maps/timeline*
// @grant        none
// @updateURL    https://github.com/eagl3s1ght/google_maps_worktime_collector/raw/master/script.user.js
// ==/UserScript==

const WORK_LOCATION_LABEL = "Feminint"

function loadjQuery( callbackFn ){
    if(typeof(jQuery)=="undefined"){
        window.jQuery="loading";
        var a=document.createElement("script");
        a.type="text/javascript";
        a.src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js";
        a.onload=function(){console.log("jQuery "+jQuery.fn.jquery+" loaded successfully."); callbackFn(); };
        a.onerror=function(){delete jQuery;console.log("Error while loading jQuery!")};
        document.getElementsByTagName("head")[0].appendChild(a)
    }else{
        if(typeof(jQuery)=="function"){console.log("jQuery ("+jQuery.fn.jquery+") is already loaded!"); callbackFn(); }else{console.log("jQuery is already loading...")}
    }
};
function save(val){ localStorage.setItem( "worktimes", JSON.stringify( val ) ); }
window.clearworktimes = function(){ localStorage.removeItem( "worktimes" ); }
function checkx(x){
    if( x == null )
        x = { "history" : [] };
    if( Array.isArray(x) == true )
        x = { "history" : [] }
    if( typeof x !== "object" )
        x = { "history" : [] }
    if( Array.isArray(x.history) == false )
        x = { "history" : [] }

    return x;
}
function load(){ return checkx( JSON.parse( localStorage.getItem( "worktimes" ) ) ) }
loadjQuery( callback )
function callback(){
    (($) => {
        const TIME_TO_WAIT = 2;

        $('body').prepend( $('<i class="btn">runback true</i>').on('click', (e) => { window.runback = true; }).css({
    "z-index": "99999999",
    "position": "relative",
}) )
        $('body').prepend( $('<i class="btn">runback false</i>').on('click', (e) => { window.runback = false; }).css({
    "z-index": "99999999",
    "position": "relative",
}) )

        scriptStart = Math.floor(Date.now() / 1000)
        lastChange = false;
        if( typeof val !== "undefined" )
            clearInterval( val )

        val = setInterval(( now = Math.floor(Date.now() / 1000) ) => {
            if( typeof debug !== "undefined" && debug === true )
                console.log("sec", {"now": now, "scriptStart": scriptStart, "lastChange": lastChange, calc: now - lastChange});

            if( typeof lastChange !== "undefined" && lastChange !== false && now - lastChange > TIME_TO_WAIT ){
                console.log( "Checking for WORK_LOCATION_LABEL = ", WORK_LOCATION_LABEL )
                lastChange = false;
                worktime()
                if(window.runback === true){
                    $('.previous-date-range-button').click()
                }

            }
        }, 1000);
        $("body").on('DOMSubtreeModified', ".timeline-wrapper", function() {
            if( lastChange == false )
                lastChange = Math.floor(Date.now() / 1000)
        });

        function worktime(){
            var found = false;
            var date;

            date =  $(".goog-date-picker-selected").attr("aria-label");
            $("html").find('div[jsinstance]').each( (i, e) => {

                if( $(e).html().includes(WORK_LOCATION_LABEL) ){
                    found = true;

                    var segments = $(e).find(".segment-duration-part")
                    y = "" + date + "," + $(segments[0]).html() +  "-" + $(segments[1]).html();
                    ymatch = y.replace(/\./,"").match(/(\d+\s[a-zA-Z]{1,3})(.*)/);
                    var cute_date = new Date(ymatch[1] + " 2019").toDateString()
                    y = y.replace(date + ",", "");
                    // console.log({date, cute_date, ymatch, y})
                    newobj = { "date": cute_date, "period": y }
                    x = load()
                    if( x.history.indexOf(newobj) === -1 ){
                        x.history.push( newobj )
                        save(x)

                    } else {
                        // already existant entry
                    }
                }
            } );

            if(!found)
                ;// no work entry found for date
            if(found){
                console.log( load().history )
                console.log(
                    ConvertToCSV(load().history)
                )

            }
        }

    })(jQuery)
}
const ConvertToCSV = (objArray) => {
    let rows = typeof objArray !== "object" ? JSON.parse(objArray)
                                            : objArray;
    let header = "";
    Object.keys(rows[0]).map(pr => (header += pr + ","));

    let str = "";
    rows.forEach(row => {
        let line = "";
        let columns = typeof row !== "object" ? JSON.parse(row)
                                              : Object.values(row);
        columns.forEach(column => {
            if (line !== "") {
                line += ",";
            }
            if (typeof column === "object") {
                line += JSON.stringify(column);
            } else {
                line += column;
            }
        });
        str += line + "\r\n";
    });
    return header + "\r\n" + str;
}
