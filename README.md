# google_maps_worktime_collector
I created this to easily and automatically extract times each day I've been at my work location.
I use the times extracted in a [Work Report spreadsheet](https://docs.google.com/spreadsheets/d/1mYcF9OwXAg-2HQq-NvdVF3nAzvUaz7r6Wbpa9Nh7xbU/edit?usp=sharing).
